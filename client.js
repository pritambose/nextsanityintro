import sanityClient  from "@sanity/client";

export default sanityClient({
    projectId:'5zootov8',
    dataset:'production'
})