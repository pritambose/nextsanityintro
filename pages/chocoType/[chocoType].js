import React, {useState, useEffect} from 'react'
import Image from 'next/image'
import {useRouter} from 'next/router'
import styles from '../../styles/Home.module.css'
import sanityClient from "../../client.js"

const Visa = () => {
    const router =useRouter()
    const chocoTypeName = router.query.chocoType
    const [pageContent, setPageContent] = useState(null)
  useEffect(()=>{
    sanityClient.fetch(`*[_type=="vendor"]{
      title,
      slug,
      description,
      logo{
        asset->{
          _id,
          url
        },
        alt
      }
    }`).then((data)=>{
      console.log('Data received', data)
      setPageContent(data)
    })
    .catch((error)=>{console.log(error)})
  },[])

  const getDescription = () =>{
    console.log('getDescription', pageContent)
    const choco = pageContent?.filter((value) =>{ return value.slug.current === chocoTypeName})
    console.log('getDescription', choco)
    return choco ? choco[0]?.description : null
  }

    return (
        <div className={styles.container}>
        <main className={styles.main}>
          <h1 className={styles.title}>
            Welcome to {chocoTypeName}!
          </h1>
          
          <div className={styles.grid}>
          <div className={styles.card}>
            <p>{getDescription()}</p>
          </div>
          </div>
        </main>
  
        <footer className={styles.footer}>
        <a>
          Loved By all
        </a>
      </footer>
      </div>
    )
  }
  export default Visa
  