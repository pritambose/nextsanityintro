import Image from 'next/image'
import Link from 'next/link'
import styles from '../../styles/Home.module.css'

const Vault = () => {
  return (
    <div className={styles.container}>
      <main className={styles.main}>
        <h1 className={styles.title}>
          Welcome to Vault!
        </h1>        
        <div className={styles.grid}>
        <Link href='/vault/visa'>
          <div className={styles.card}>
            <h2>Route to VISA &rarr;</h2>
            <p>Details of VISA</p>
          </div>
          </Link>
          <Link href='/vault/mastercard'>
          <div className={styles.card}>
            <h2>Route to MASTERCARD &rarr;</h2>
            <p>Details of MASTERCARD</p>
          </div>
          </Link>
        </div>
      </main>

      <footer className={styles.footer}>
        <a>
          Loved By all
        </a>
      </footer>
    </div>
  )
}
export default Vault
