import React, {useState, useEffect} from 'react'
import Head from 'next/head'
import { useRouter } from 'next/router'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import sanityClient from "../client.js"

export default function Home() {
  const router = useRouter()
  const navigateToDetails = (value) => {    
    router.push(`/chocoType/${value}`)
  }
  const [pageContent, setPageContent] = useState(null)
  useEffect(()=>{
    sanityClient.fetch(`*[_type=="vendor"]{
      title,
      slug,
      description,
      logo{
        asset->{
          _id,
          url
        },
        alt
      }
    }`).then((data)=>{
      console.log('Data received', data)
      setPageContent(data)
    })
    .catch((error)=>{console.log(error)})
  },[])
  
  return (
    <div className={styles.container}>
      <Head>
        <title>NextKit</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          Welcome to Chocolate Factory!
        </h1>        

        <div className={styles.grid}>
          {console.log("value", pageContent)}
          {pageContent && pageContent.map((content,index) => (
            <div onClick={()=>{navigateToDetails(content.slug.current)}} className={styles.card} key={index}>
            <div className={styles.round_1}>
            <img className={styles.imageHolder} width='88%' margin-top='14px' src={content.logo.asset.url}></img>
            </div>
            <h2>{content.title} land &rarr;</h2>
            <p>Insight on {content.slug.current}</p>
            <p className={styles.elli}>{content.description}</p>
          </div>
          ))}
        </div>
      </main>

      <footer className={styles.footer}>
        <a>
          Loved by all
        </a>
      </footer>
    </div>
  )
}


